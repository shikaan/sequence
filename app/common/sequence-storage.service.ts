import { Injectable } 	from '@angular/core';
import { Sequence }		from '../model/sequence.class';
import { Bar }			from '../model/bar.class';
import { Chord }		from '../model/chord.class';
import { Note }			from '../model/note.class';
import {SequenceStorageEntry} from "../model/sequence-storage-entry.class";

@Injectable()
export class SequenceStorageService {
	constructor() {}

	private SequenceToJSON(sequence: Sequence){
		let bars : {}[] = [];

		sequence.bars.forEach((bar: Bar) => {
			bars.push(this.BarToJSON(bar));
		});

		return {
			title: sequence.title,
			bars: bars,
			signature: sequence.signature
		}
	}

	private BarToJSON(bar:Bar){
		let chords : {}[] = [];

		bar.chords.forEach((chord: Chord) => {
			chords.push(this.ChordToJSON(chord));
		});

		return {
			index: bar.index,
			chords: chords,
			numberOfBeats: bar.numberOfBeats
		}
	}

	private ChordToJSON(chord: Chord){
		let notes : {}[] = [];

		chord.notes.forEach((note: Note) =>{
			notes.push(this.NoteToJSON(note))
		});

		return {
			symbol: chord.symbol,
			notes: notes,
			durationInBeats: chord.durationInBeats
		}
	}

	private NoteToJSON(note: Note){
		return {value: note.value, name: note.name}
	}

	private JSONtoSequence(sequenceJSON: any){
		let parsedSequence = new SequenceStorageEntry(sequenceJSON.signature, sequenceJSON.title);

		sequenceJSON.bars.forEach((barJSON: any) => {
			parsedSequence.addBar(this.JSONtoBar(barJSON));
		});

		parsedSequence.date = (new Date(sequenceJSON.date));

		return parsedSequence;
	}

	private JSONtoBar(barJSON: any){
		let parsedBar = new Bar(barJSON.numberOfBeats);

		barJSON.chords.forEach((chordJSON: any) => {
			parsedBar.addChord(this.JSONtoChord(chordJSON));
		});

		parsedBar.numberOfBeats = barJSON.numberOfBeats;

		return parsedBar;
	}

	private JSONtoChord(chordJSON: any){
		let notes : Note[] = [];

		chordJSON.notes.forEach((noteJSON: any) => {
			notes.push(this.JSONtoNote(noteJSON))
		});

		return new Chord(chordJSON.symbol, chordJSON.durationInBeats, notes);
	}

	private JSONtoNote(noteJSON: any){
		return new Note(noteJSON.value);
	}

	public saveSequence(sequence: Sequence){
		let sequences = JSON.parse(localStorage.getItem("sequences")) || {};

		sequences[sequence.title] = this.SequenceToJSON(sequence);
		sequences[sequence.title].date = Date.now();

		localStorage.setItem("sequences", JSON.stringify(sequences));
	}

	public getSequenceList(){
		let sequences = JSON.parse(localStorage.getItem("sequences"));
		let sequencesMap: Map<string, SequenceStorageEntry> = new Map();

		for(let key in sequences){
		    let sequenceEntry = this.JSONtoSequence(sequences[key]);
            sequencesMap.set(sequences[key].title, sequenceEntry);
        }

        return sequencesMap;
	}

	public deleteSequence(sequenceTitle: string){
        let sequences = JSON.parse(localStorage.getItem("sequences"));
        delete sequences[sequenceTitle];
        localStorage.setItem("sequences", JSON.stringify(sequences));
    }
}