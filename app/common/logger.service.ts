import { Injectable } from '@angular/core'

enum LOG_LEVEL {
	NONE,	//none
	INFO,	//Provides additional feedback to the application ones 
	WARN, 	//INFO + warnings
	DEBUG	//WARN + errors
}

@Injectable()
export class LoggerService {
	private _level : LOG_LEVEL;

	constructor(){
		this._level = this.getLevelFromQueryString();
	}

	private getLevelFromQueryString(){
		//TDD
		//Read from querystring a parameter associated to a log level 
		//and displays only the desired logging
		return LOG_LEVEL.DEBUG
	}

	private printLog(type: LOG_LEVEL, anyMessage: any){
		let message = JSON.stringify(anyMessage);
		switch(type){
			case LOG_LEVEL.DEBUG:
				console.log('%c --- ERROR --- ', 'background: red; color: white; font-weight: 700');
				console.log('%c' + message, 'color: red; font-weight: 700');
				break;
			case LOG_LEVEL.WARN:
				console.log('%c --- WARNING --- ', 'background: yellow; color: Black; font-weight: 700');
				console.log('%c' + message, 'color: yellow; background: black; font-weight: 700');
				break;
			case LOG_LEVEL.INFO:
				console.log('%c' + message, 'color: blue');
				break;
		}
	}

	error(message: any){
		if(this._level >= LOG_LEVEL.DEBUG)
			this.printLog(LOG_LEVEL.DEBUG, message);
	}

	warning(message: any){
		if(this._level >= LOG_LEVEL.WARN)
			this.printLog(LOG_LEVEL.WARN, message);
	}

	info(message: any){
		if(this._level >= LOG_LEVEL.INFO)
			this.printLog(LOG_LEVEL.INFO, message);
	}

}