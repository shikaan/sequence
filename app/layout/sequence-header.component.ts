import { Component, Input } from '@angular/core';
import { MdlDialogService }    	from "angular2-mdl";

import { MidiPlayerService } 	from '../midi-engine/midi-player.service';
import { LoggerService } 	from '../common/logger.service';
import { SequenceStorageService } 	from '../common/sequence-storage.service';

import { Sequence } from '../model/sequence.class';
import {PlayerSettingsComponent} from '../dialogs/player-settings.component'
import {SequenceSettingsComponent} from '../dialogs/sequence-settings.component'
import {OpenSequenceComponent} from '../dialogs/open-sequence.component'
import {SequenceStorageEntry} from "../model/sequence-storage-entry.class";
import {Bar} from "../model/bar.class";
import {Chord} from "../model/chord.class";
import {DeleteSequenceComponent} from "../dialogs/delete-sequence.component";

@Component({
	moduleId: module.id,
	selector: 'sequence-header',
	templateUrl: './sequence-header.template.html'
})

export class SequenceHeaderComponent{
	@Input() sequence: Sequence;

	constructor(
		private dialogService: MdlDialogService,
		private sequenceStorageService: SequenceStorageService,
		public player: MidiPlayerService
	){}

	showPlayerSettingsDialog() {
        this.dialogService.showCustomDialog({
            component: PlayerSettingsComponent,
            providers: [
				{provide: 'MidiPlayerService', useValue: this.player}
			],
            styles: {'min-width': '361px'},
            isModal: true,
        });
    }

    showSequenceSettingsDialog() {
        this.dialogService.showCustomDialog({
            component: SequenceSettingsComponent,
            providers: [{provide: 'currentSequence', useValue: this.sequence}, LoggerService],
            styles: {'min-width': '361px'},
            isModal: true,
        });
    }

    showOpenSequenceDialog(){
        let sequences = this.sequenceStorageService.getSequenceList();

        /**
         * TODO: remove this once Angular2 will support iterating over maps...
         */
        let sequencesArray : SequenceStorageEntry[] = [];
        sequences.forEach((sequence) =>{
            sequencesArray.push(sequence);
        });

        this.dialogService.showCustomDialog({
            component: OpenSequenceComponent,
            providers:[
                {provide: 'sequencesArray', useValue: sequencesArray},
                {provide: 'sequences', useValue: sequences},
                {provide: 'currentSequence', useValue: this.sequence},
                {provide: 'sequenceStorageService', useValue: this.sequenceStorageService}
            ],
            styles: {'min-width': '361px'},
            isModal: true,
        });
    }

    showDeleteSequenceDialog(){
        let sequences = this.sequenceStorageService.getSequenceList();

        /**
         * TODO: remove this once Angular2 will support iterating over maps...
         */
        let sequencesArray : SequenceStorageEntry[] = [];
        sequences.forEach((sequence) =>{
            sequencesArray.push(sequence);
        });

        this.dialogService.showCustomDialog({
            component: DeleteSequenceComponent,
            providers:[
                {provide: 'sequencesArray', useValue: sequencesArray},
                {provide: 'sequences', useValue: sequences},
                {provide: 'currentSequence', useValue: this.sequence},
                {provide: 'sequenceStorageService', useValue: this.sequenceStorageService}
            ],
            styles: {'min-width': '361px'},
            isModal: true,
        });
    }

    saveSequence(){
    	this.sequenceStorageService.saveSequence(this.sequence);
    }

    newSequence(){
        let newSequence = new Sequence();
        let newBar = new Bar(4);
        let newChord = new Chord('-');

        newBar.addChord(newChord);
        newSequence.addBar(newBar);

        this.sequence.title = newSequence.title;
        this.sequence.bars = newSequence.bars;
        this.sequence.signature = newSequence.signature;
    }
}