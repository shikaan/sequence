import { Component, Input } from '@angular/core';

@Component({
	moduleId: module.id,
	selector: 'overlay',
	templateUrl: 'overlay.template.html',
	styleUrls: [
		'overlay.styles.css'
	]
})

export class OverlayComponent {
	@Input() loading : boolean;
}