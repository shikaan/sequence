import {Component, Inject}   from '@angular/core';
import {MidiSetupService}    from './midi-engine/midi-setup.service';

import {MidiPlayerService}   from './midi-engine/midi-player.service';
import {LoggerService}       from './common/logger.service';
import {SequenceStorageService}       from './common/sequence-storage.service';

import {Chord}               from './model/chord.class'
import {Bar}                 from './model/bar.class'
import {Sequence}            from './model/sequence.class'

@Component({
    moduleId: module.id,
    selector: 'app',
    templateUrl: 'app.template.html',
    providers: [
        MidiSetupService,
        MidiPlayerService,
        SequenceStorageService,
        LoggerService,
        {provide: 'currentSequence', useValue: new Sequence()}
    ]
})
export class AppComponent {
    public sequence: Sequence;
    public ready: boolean = false;

    constructor(
        private _midiSetup: MidiSetupService,
        @Inject('currentSequence') sequence:Sequence
    ) {
        this.sequence = sequence;
        this._midiSetup.setup();
        document.addEventListener('MIDI READY', () => this.ready = true);
    }
}