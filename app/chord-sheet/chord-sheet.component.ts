import { Component, Input, OnChanges } 	from '@angular/core';

import { Sequence } 			from '../model/sequence.class';
import { Bar } 					from '../model/bar.class';

import { MidiPlayerService } 	from '../midi-engine/midi-player.service';
import { LoggerService }        from '../common/logger.service';
import { ChordSheetService } 	from './chord-sheet.service';


@Component({
	moduleId: module.id,
	selector: 'chord-sheet',
	templateUrl: './chord-sheet.template.html',
	providers: [
		ChordSheetService
	],
	styleUrls: [
		'chord-sheet.style.css'
	]
})

export class ChordSheetComponent implements OnChanges {
	@Input() sequence : Sequence;

	public playIcon : boolean = true;
	private _sequenceLength = 0;
	private _timeoutID: any;

	constructor(
		public player: MidiPlayerService, 
		public logger: LoggerService,
		public service: ChordSheetService
	){
	};
	
	playCurrentSequence(){
		this._sequenceLength = this.player.playSequence(this.sequence);
		this.playIcon = false;
		this._timeoutID = setTimeout(() => {
			this.stopCurrentSequence();
		}, this._sequenceLength);
	}

	stopCurrentSequence(){
		this.player.stopSequence();
		this.playIcon = true;
		clearTimeout(this._timeoutID);
	}

	updateSequence(bar: Bar){
		this.logger.info('Now parsing: ' + bar.chords[0].symbol);
		if(bar.previous){
			let last_chord = bar.previous.chords[bar.previous.chords.length - 1];
			bar.chords[0].notes = this.service.getNearestNotePosition(last_chord, bar.chords[0]);
		}
		else{
			bar.chords[0].notes = bar.chords[0].getInversions()[0];
		}
		
		if(bar.next !== null)
			this.updateSequence(bar.next);
	}

	ngOnChanges(){
		if(!!this.sequence.bars.length)
			this.updateSequence(this.sequence.bars[0]);
	}
}
