import { Injectable } from '@angular/core'
import { Chord } from '../model/chord.class'
import { Note } from '../model/note.class'

@Injectable()
export class ChordSheetService{

	private _getNotesDistance(notes1: Note[], notes2: Note[]): number{
		//this is a simple euclidean metric implementation
		let x2 = Math.pow(notes1[0].value - notes2[0].value, 2);
		let y2 = Math.pow(notes1[1].value - notes2[1].value, 2);
		let z2 = Math.pow(notes1[2].value - notes2[2].value, 2);

		return Math.sqrt(x2 + y2 + z2)
	}

	private _getTranslatedPositions(root_notes: Note[]){
		let notes : Note[][] = [];
		notes[0] = root_notes;
		notes[1] = [];
		notes[2] = [];
		
		root_notes.forEach((note : Note) => {
			notes[1].push(new Note(note.value - 12)); //one octave below
			notes[2].push(new Note(note.value + 12));	//one octave above
		})

		return notes;
	}

	getNearestNotePosition(first_chord: Chord, second_chord: Chord){
		let all_inversions_position : Note[][] = [];

		let inversions = second_chord.getInversions();

		inversions.forEach((inversion : Note[]) => {
			all_inversions_position = all_inversions_position.concat(this._getTranslatedPositions(inversion))
		})

		let minimum = 0;
		let minimum_index = 0;
		for( let i = 0; i < all_inversions_position.length; i++){
			let distance = this._getNotesDistance(first_chord.notes, all_inversions_position[i]);
			if(distance === 0){
				return all_inversions_position[i]
			}
			if(distance < minimum || minimum == 0){
				minimum = distance;
				minimum_index = i;
			}
		}

		return all_inversions_position[minimum_index];
	}
}