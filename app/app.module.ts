import { NgModule, APP_INITIALIZER }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { MdlModule } from 'angular2-mdl';

import { MidiJSWrapperService } from './midi-engine/midijs-wrapper.service';

import { AppComponent }  from './app.component';
import { SequenceHeaderComponent }  from './layout/sequence-header.component';
import { PlayerSettingsComponent } from './dialogs/player-settings.component';
import { OpenSequenceComponent } from './dialogs/open-sequence.component';
import { DeleteSequenceComponent } from './dialogs/delete-sequence.component';
import { SequenceSettingsComponent } from './dialogs/sequence-settings.component';
import { ChordSheetComponent } from './chord-sheet/chord-sheet.component';
import { OverlayComponent } from './overlay/overlay.component';

@NgModule({
	imports: [ 
		BrowserModule, 
		FormsModule,
		MdlModule
	],
	providers: 	[
		MidiJSWrapperService,
  		{
			provide: APP_INITIALIZER,
			useFactory: (midiWrapper: MidiJSWrapperService) => () => midiWrapper.init(),
			deps: [MidiJSWrapperService],
			multi: true
		}
	],
	declarations: [ 
		AppComponent,
		SequenceHeaderComponent,
		PlayerSettingsComponent,
		OpenSequenceComponent,
		DeleteSequenceComponent,
		SequenceSettingsComponent,
		ChordSheetComponent,
		OverlayComponent
	],
	entryComponents: [
		PlayerSettingsComponent,
		SequenceSettingsComponent,
		DeleteSequenceComponent,
		OpenSequenceComponent
	],
	bootstrap:    [ AppComponent ]
})
export class AppModule { }