import { Component, Inject } from '@angular/core';
import { MdlDialogReference } 	from 'angular2-mdl';
import {Sequence} from "../model/sequence.class";
import {SequenceStorageService} from "../common/sequence-storage.service";
import {SequenceStorageEntry} from "../model/sequence-storage-entry.class";

@Component({
	moduleId: module.id,
	selector: 'open-sequence',
	templateUrl: './open-sequence.template.html'
})

export class OpenSequenceComponent {
	sequencesArray: Array<SequenceStorageEntry>;
	sequences: Map<string, SequenceStorageEntry>;
	sequence: Sequence;
	chosen: string;

	constructor(
		private dialog: MdlDialogReference,
		@Inject('sequencesArray') sequencesArray: Array<SequenceStorageEntry>,
		@Inject('sequences') sequences: Map<string, SequenceStorageEntry>,
		@Inject('currentSequence') sequence: Sequence)
	{
		this.sequencesArray = sequencesArray;
		this.sequences = sequences;
		this.sequence = sequence;
	}

	close(){
		this.dialog.hide();
	}

	openSequence(){
		let openedSequence = this.sequences.get(this.chosen);
		this.sequence.title = openedSequence.title;
		this.sequence.bars = openedSequence.bars;
		this.sequence.signature = openedSequence.signature;
		this.dialog.hide();
	}
}