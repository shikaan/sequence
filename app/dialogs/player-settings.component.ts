import { Component, Inject } 	from '@angular/core';
import { Sequence } 			from '../model/sequence.class';
import { MidiPlayerService } 	from '../midi-engine/midi-player.service';
import { MdlDialogReference } 	from 'angular2-mdl';

@Component({
	moduleId: module.id,
	selector: 'player-settings',
	templateUrl: './player-settings.template.html'
})

export class PlayerSettingsComponent {
    player: MidiPlayerService;

    constructor(private dialog: MdlDialogReference,
				@Inject('MidiPlayerService') player: MidiPlayerService
    ){
		this.player = player;
    }

    close(){
        this.dialog.hide();
    }
}