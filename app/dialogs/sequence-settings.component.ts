import { Component, Inject } from '@angular/core';
import {LoggerService} from '../common/logger.service';
import { Sequence } from '../model/sequence.class';
import { Bar } from '../model/bar.class';
import { MdlDialogReference } from 'angular2-mdl';

@Component({
	moduleId: module.id,
	selector: 'sequence-settings',
	templateUrl: './sequence-settings.template.html'
})

export class SequenceSettingsComponent {
    public currentSequence : Sequence;
    private _initialLength : number;
    private _initialNumberOfBeats: number;
    public length : number;

    constructor(
        private logger: LoggerService,
        @Inject('currentSequence') currentSequence: Sequence,
        private dialog : MdlDialogReference
    ){
        this.currentSequence = currentSequence;
        this._initialLength = currentSequence.bars.length;
        this.length = currentSequence.bars.length;
        this._initialNumberOfBeats = currentSequence.signature[0];
    }

    save(){
        this._updateSequenceLength();
        this._updateSequenceSignature();
        this.dialog.hide();
    }

    close(){
        this.dialog.hide();
    }

    private _updateSequenceLength() {
        this.logger.info('Update sequence length');
        if(this.length == this._initialLength)
            return;
        
        if(this.length < this._initialLength){
            this.logger.info('Removing bars ' + (this.length - this._initialLength));
            this.currentSequence.bars.splice(this.length - this._initialLength);
            return
        }

        if(this.length > this._initialLength){
            let delta = this.length - this._initialLength;
            this.logger.info('Adding bars ' + delta);
            for(let i = 0; i < delta; i++){
                this.currentSequence.addBar(new Bar(this.currentSequence.signature[0]));
            }
            return
        }

    }

    private _updateSequenceSignature() {
        this.logger.info('Update sequence signature');
        if (this._initialNumberOfBeats !== this.currentSequence.signature[0]) {
            this.currentSequence.bars.forEach((bar) => {
                bar.chords[0].durationInBeats = this.currentSequence.signature[0];
                bar.numberOfBeats = this.currentSequence.signature[0]
            })
        }
    }
}