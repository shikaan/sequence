import { Component, Inject } from '@angular/core';
import { MdlDialogReference } 	from 'angular2-mdl';
import {Sequence} from "../model/sequence.class";
import {SequenceStorageService} from "../common/sequence-storage.service";
import {SequenceStorageEntry} from "../model/sequence-storage-entry.class";

@Component({
	moduleId: module.id,
	selector: 'delete-sequence',
	templateUrl: './delete-sequence.template.html'
})

export class DeleteSequenceComponent {
	sequencesArray: Array<SequenceStorageEntry>;
	sequences: Map<string, SequenceStorageEntry>;
	sequence: Sequence;
	sequenceStorageService: SequenceStorageService;
	chosen: string;

	constructor(
		private dialog: MdlDialogReference,
		@Inject('sequenceStorageService') sequenceStorageService: SequenceStorageService,
		@Inject('sequencesArray') sequencesArray: Array<SequenceStorageEntry>,
		@Inject('sequences') sequences: Map<string, SequenceStorageEntry>,
		@Inject('currentSequence') sequence: Sequence)
	{
		this.sequenceStorageService = sequenceStorageService;
		this.sequencesArray = sequencesArray;
		this.sequences = sequences;
		this.sequence = sequence;
	}

	close(){
		this.dialog.hide();
	}

	deleteSequence(){
		this.sequenceStorageService.deleteSequence(this.chosen);
		this.dialog.hide();
	}
}