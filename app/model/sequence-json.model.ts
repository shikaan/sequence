class NoteJSON{
	public value: number;
    public name: string;
}

class ChordJSON {
	public symbol : string;
	public durationInBeats : number;
	public notes : NoteJSON[];
}

class BarJSON {
	public numberOfBeats: number;
	public chords : ChordJSON[];
	public index : number;
}

export class SequenceJSON {
	title : string;
	signature : [number, number];
	bars: BarJSON[];
}