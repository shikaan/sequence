const names = {'C' : 0, 'C#': 1 , 'Db': 1 , 'D': 2, 'Eb': 3, 'D#': 3, 'E': 4, 'F': 5, 'F#': 6, 'Gb': 6, 'G': 7, 'G#': 8, 'Ab': 8, 'A': 9, 'Bb': 10, 'A#': 10, 'B': 11};
const values = ['C', 'C#', 'D', 'Eb', 'E', 'F', 'F#', 'G', 'G#', 'A', 'Bb', 'B'];

export class Note {
    private _value: number;
    private _name: string;

    get value() {
        return this._value;
    }

    set value(value: any) {
        if(!isNaN(Number(value)) && !(value < 36 || value > 91)){
			this._value = value
		}
		else if (typeof value === "string"){
			let notes = Object.keys(names);
			if(notes.indexOf(value) > -1) {
				this._value = (names[value]) + 60; //starting from central C 
			}
			else{
				this._value = 36	
			}
		}
		else{
			this._value = 36
		}
    }

    get name() {
    	let normalizedValue : number = this._value % 12;
    	return values[normalizedValue];
    }

    constructor(value: any){
    	this.value = value;
    }
}