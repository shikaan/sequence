import {Bar} from "./bar.class";
import {Chord} from "./chord.class";

export class Sequence {
	private _bars 			: Bar[];
	public signature		: [number, number];
	public title 			: string;

	get bars(){
		return this._bars;
	}
	
	set bars(bars : Bar[]) {
		this._bars = bars;
	}

	constructor(signature?: [number, number], title?: string){
		this._bars = [];

        this.signature = signature || [4, 4];
        this.title = title || "New Sequence";
	}

	addBar(bar: Bar){
		if(bar.numberOfBeats > this.signature[0]){
			throw 'Invalid bar' 
		}
		else{
			let lastBar = this._bars[this._bars.length - 1];
			
			if(lastBar){
				lastBar.next = bar;
				bar.previous = lastBar;
			}
			
			bar.index = this._bars.length + 1;

            if (!bar.chords.length) {
                bar.addChord(new Chord('-'));
            }
			this._bars.push(bar);
		}
	}
}