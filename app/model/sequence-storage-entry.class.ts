import {Sequence} from "./sequence.class";

export class SequenceStorageEntry extends Sequence{
    date: Date;
}