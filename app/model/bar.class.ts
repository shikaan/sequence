import { Chord } from './chord.class'

export class Bar {
	public numberOfBeats: number;
	private _chords : Chord[];
	public index : number;
	public next: Bar;
	public previous: Bar;


	get chords(){
		return this._chords;
	}

	constructor(length: number){
		this.numberOfBeats = length;
		this._chords = [];
		this.next = null;
	}

	addChord(chord: Chord){
		let totalLength = 0;
		this.chords.forEach(function(item){
			totalLength = totalLength + item.durationInBeats;
		});

		if (totalLength + chord.durationInBeats > this.numberOfBeats)
			throw 'Chord sequence too long';
		else{
			this._chords.push(chord);
		}
	}
}