import { Note } from './note.class';
import { ChordFactory } from './chord.factory';

export class Chord {
	public symbol : string;
	public durationInBeats : number;
	public notes : Note[];

    constructor(symbol?: string, length?: number, notes?: Note[]) {
		this.durationInBeats = length || 4;
        this.symbol = symbol || '';
        this.notes = notes || this.getInversions()[0];
	}

	getInversions(){
		switch(this.symbol.length){
			case 1:
				return ChordFactory.oneDigitSymbol(this.symbol);
			case 2:
				return ChordFactory.twoDigitsSymbol(this.symbol);
			case 3:
				return ChordFactory.threeDigitsSymbol(this.symbol);
			case 4:
				return ChordFactory.fourDigitsSymbol(this.symbol);
			default:
                return []
		}
	}
	
	getNotesValue(){
		let aux : number[] = [];
		this.notes.forEach((item) => {
			aux.push(item.value);
        });
		
		return aux;
	}
}