interface PlayerOptionsInterface {
	volume: number
	bpm: number
	enableMetronome: boolean
	loops: number
} 

function boundProperty(propertyValue: number, upperBound: number, lowerBound: number){
	if(propertyValue < lowerBound)
		return lowerBound;
	if(propertyValue > upperBound)
		return upperBound;
	return propertyValue;
}

export class PlayerOptions implements PlayerOptionsInterface {
	private _volume : number;
	private _bpm : number;
	private _loops : number;
	public enableMetronome: boolean;

	get volume(){
		return this._volume;
	}

	set volume(value: number){
		this._volume = boundProperty(value, 127, 0);
	}

	get bpm(){
		return this._bpm;
	}

	set bpm(value: number){
		this._bpm = boundProperty(value, 300, 20);
	}

	get loops(){
		return this._loops;
	}

	set loops(value: number){
		this._loops = boundProperty(value, 20, 1);
	}

	constructor(){
		this._volume = 127;
		this._bpm = 90;
		this._loops = 1;
		this.enableMetronome = false; 
	}
}