import { Note } from './note.class';

function getMajorTriadInversions(root: Note) {
	let root_value = root.value;

	let root_position = [root, new Note(root_value + 4), new Note(root_value + 7)];
	let first_inversion = [new Note(root_value - 5), root, new Note(root_value + 4)];
	let second_inversion = [new Note(root_value - 8), new Note(root_value - 5), root];

	return [root_position, first_inversion, second_inversion]
}

function getMinorTriadInversions(root: Note) {
	let root_value = root.value;

	let root_position_m = [root, new Note(root_value + 3), new Note(root_value + 7)];
	let first_inversion_m = [new Note(root_value - 5), root, new Note(root_value + 3)];
	let second_inversion_m = [new Note(root_value - 9), new Note(root_value - 5), root];
	return [root_position_m, first_inversion_m, second_inversion_m];
}

function getDominantSeventhInversions(root: Note) {
	let root_value = root.value;

	let root_position_7 = [root, new Note(root_value + 4), new Note(root_value + 10)];
	let first_inversion_7 = [new Note(root_value - 2), root, new Note(root_value + 4)];
	let second_inversion_7 = [new Note(root_value - 8), new Note(root_value - 2), root];
	return [root_position_7, first_inversion_7, second_inversion_7];
}

function getMinorSeventhInversions(root: Note) {
	let root_value = root.value;

	let root_position_m7 = [root, new Note(root_value + 3), new Note(root_value + 10)];
	let first_inversion_m7 = [new Note(root_value - 2), root, new Note(root_value + 3)];
	let second_inversion_m7 = [new Note(root_value - 9), new Note(root_value - 2), root];
	return [root_position_m7, first_inversion_m7, second_inversion_m7];
}

export class ChordFactory {
	/**
	 * Returns a major chord in root position, when a symbol prompted.
	 * @param {string} symbol [description]
	 */
	static oneDigitSymbol(symbol: string) {
		if (symbol.length > 1)
			return;

		let root: Note = new Note(symbol);

		return getMajorTriadInversions(root);
	}
	/**
	 * [function description]
	 * @type {[type]}
	 */
	static twoDigitsSymbol(symbol: string) {
		if (symbol.length !== 2)
			return;

		let root: Note = new Note(symbol[0]);
		let root_value = root.value;

		switch (symbol[1]) {
			case 'b':
				let flat = new Note(root_value-1);
				return getMajorTriadInversions(flat);
			case '#':
				let sharp = new Note(root_value+1);
				return getMajorTriadInversions(sharp);
			case 'm':
			case '-':
				return getMinorTriadInversions(root);
			case '7':
				return getDominantSeventhInversions(root);
			default:
				return [];

		}
	}

	/**
	  * [function description]
	  * @type {[type]}
	  */
	static threeDigitsSymbol(symbol: string) {
		if (symbol.length !== 3)
			return;

		let root: Note = new Note(symbol[0]);
		let root_value = root.value;

		switch (symbol.slice(-2)) {
			case 'b7':
				let flat = new Note(root_value-1);
				return getDominantSeventhInversions(flat);
			case '#7':
				let sharp = new Note(root_value+1);
				return getDominantSeventhInversions(sharp);
			case 'm7':
			case '-7':
				return getMinorSeventhInversions(root);
			default:
				return [];

		}
	}
	
	/**
	  * [function description]
	  * @type {[type]}
	  */
	static fourDigitsSymbol(symbol: string) {
		if (symbol.length !== 4)
			return;

		let root: Note = new Note(symbol[0]);
		let root_value = root.value;

		switch (symbol.slice(-3)) {
			case 'bm7':
				let flat = new Note(root_value-1);
				return getMinorSeventhInversions(flat);
			case '#m7':
				let sharp = new Note(root_value+1);
				return getMinorSeventhInversions(sharp);
			default:
				return [];
		}
	}
}