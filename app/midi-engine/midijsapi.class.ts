/**
 * Class which wraps the MidiJS API
 */

export class MidiJSApi {
	MIDI : any;
	dom : any;

	constructor(MIDI : any, dom : any ){
		this.MIDI = MIDI;
		this.dom = dom;
	}
}