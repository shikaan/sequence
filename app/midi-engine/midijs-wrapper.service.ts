/**
 * Responsibility:
 * 	* Wrap MidiJS Api
 * 	(not the setup, not the playing)
 */


declare var System: any;

import { Injectable } from '@angular/core';
import { MidiJSApi } from './midijsapi.class';

@Injectable()
export class MidiJSWrapperService {
	private _MidiJSWrapper : MidiJSApi;

	getMidi() : any {
		return this._MidiJSWrapper.MIDI;
	}

	init() : Promise<MidiJSApi> {
		let promiseChain : Promise<any>[] = [];

		promiseChain.push(System.import('vendor/MIDI/inc/shim/Base64.js'));
		promiseChain.push(System.import('vendor/MIDI/inc/shim/Base64binary.js'));
		promiseChain.push(System.import('vendor/MIDI/inc/shim/WebAudioAPI.js'));

		return Promise.all(promiseChain)
			.then( () => {
				return System.import('vendor/MIDI/MIDI.min.js')
                    .then((data: any) => {
						this._MidiJSWrapper = new MidiJSApi(data.MIDI, data.dom);
					})
                    .catch((error: any) => {
						console.error(error);
					})
			});
	}
}