import { Injectable } from '@angular/core';
import { MidiJSWrapperService } from './midijs-wrapper.service'
import { Chord } from '../model/chord.class';
import { Bar } from '../model/bar.class';
import { Sequence } from '../model/sequence.class';
import { PlayerOptions } from '../model/player-options.class';
import { EventQueue } from './event-queue.class';

@Injectable()
export class MidiPlayerService {
	private _midi :  any;
	private _queue : EventQueue;
	private _metronomeInterval : any;
	public options : PlayerOptions;

	constructor(private MidiService: MidiJSWrapperService){
		this._midi = MidiService.getMidi();
		this.options = new PlayerOptions();
		this._queue = new EventQueue();
	}

	playChord(chord: Chord, delayInBeats?: number) {
		let durationInMilliseconds = chord.durationInBeats*this.beatLengthInMilliseconds();
		let delayInMilliseconds = 0;

		if(delayInBeats != null)
			delayInMilliseconds = delayInBeats * this.beatLengthInMilliseconds();

		let timer = setTimeout(() => {
			this._midi.chordOn(0, chord.getNotesValue(), this.options.volume, 0);
    		this._midi.chordOff(0, chord.getNotesValue(), durationInMilliseconds);
    		this._queue.dequeue()
    	}, delayInMilliseconds);

		let timerID = Number(timer);

		this._queue.enqueue(timerID);
	}

	playBar(bar: Bar, delayInBeats?: number){
		let progress : number = delayInBeats;
		bar.chords.forEach((chord) => {
			this.playChord(chord, progress);
			progress = progress + chord.durationInBeats;
		})
	}

	/**
	 * @param  {Sequence} sequence [description]
	 * @return {number}            Length in ms of the sequence
	 */
	playSequence(sequence: Sequence) : number {
		let auxBars = sequence.bars;
		
		for(let i = 1; i < this.options.loops; i++){
			auxBars = auxBars.concat(sequence.bars);
		}
		
		if(this.options.enableMetronome)
			this._playMetronome();
		
		let progress : number = 0;
		auxBars.forEach((bar) => {
			this.playBar(bar, progress);
			progress = progress + bar.numberOfBeats;
		});

		return progress*this.beatLengthInMilliseconds();
	}

	stopSequence(){
		this._queue.clearNow();
		if(this._metronomeInterval !== 0)
			this._stopMetronome();
	}
	
	_playMetronome(){
		let player = this;
		this._metronomeInterval = setInterval(function() {
			player._midi.noteOn(1, 60, player.options.volume, 0);
			player._midi.noteOff(1, 60, 50);
		}, this.beatLengthInMilliseconds())
	}
	
	_stopMetronome(){
		clearInterval(this._metronomeInterval);
		this._metronomeInterval = 0;
	}

	beatLengthInMilliseconds(){
		return 60000/this.options.bpm;
	}
}