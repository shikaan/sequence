export class EventQueue extends Array<number> {
    enqueue(id: number){
    	this.push(id);
    }

    dequeue(){
    	return this.splice(0, 1)[0]
    }

    clearNow(){
    	this.forEach((item) => {
    		clearInterval(item);
    	})
    	this.splice(0);
    }
}