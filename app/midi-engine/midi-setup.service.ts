import { Injectable } from '@angular/core';
import { MidiJSWrapperService } from './midijs-wrapper.service'

@Injectable()
export class MidiSetupService {
	private _midi : any;
	private _volume : number

	get volume(){
		return this._volume;
	}

	constructor(private MidiService: MidiJSWrapperService){
		this._midi = MidiService.getMidi();
		//TODO: this in a configuration file 
		this._volume = 127;
	}

	setup() : void {
		let service = this;
		this._midi.loadPlugin({
            instruments: ["acoustic_grand_piano", "woodblock"],
            onsuccess: function(){
            	service.setVolume(service.volume);
				service._fixMidiChannels();
				document.dispatchEvent(new Event('MIDI READY'));
			}			
		})
	}

	setVolume(volume: number){
		this._midi.setVolume(0, volume);
	}
	
	/**
	*	This will fix MIDI.js channels mess...
	*	PLEASE NOTE:
	*	This channel trick could be useful in case you want to use 
	*	more than one instrument per sequence.
	*/ 
	private _fixMidiChannels(){
		let woodblockID = this._midi.GM.byName["woodblock"].number;
		let pianoID = this._midi.GM.byName["acoustic_grand_piano"].number;
		
		this._midi.channels[0].instrument = pianoID;
		this._midi.channels[1].instrument = woodblockID;
	}
}