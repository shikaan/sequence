# Features:

* Chord Sheet:

    * One chord per bar;

    * Edit and validate chords;

    * Legame Armonico;

* UI:
    
    * Menu:
	
       * Open and save sequence;
    
    * Nav:
        * Player settings (speed, volume, metronome enabled)

        * Sequence Settings (name, signature)   

# Missing to be ready to deploy:

* Capability to save and open sequences;

* Tests of nay kind...;

# Possible improvements:

* More than one chord per bar (with a dialog or with four text fields per bar);

* Current playing position cursor;

* Handle other kind of chords;

* Handle other kind of signature;

* Add more intruments;

* Add styles support (swing, bebop, funk, rnb...):
    
    * Add multilayer instrument play;

    * Add a "real" tempo subdivision (probably this will be better done after adding more chords per bar);

# To dos:
    
    * Tests;

    * Improve separation of concerns in app component;
    